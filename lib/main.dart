import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:digital_krishi/UI/AdminScreens/adminMainScreen.dart';
import 'package:digital_krishi/UI/AuthScreens/loginScreen.dart';
import 'package:digital_krishi/UI/AuthScreens/signUpScreen.dart';
import 'package:digital_krishi/UI/ExpertScreens/expertMainScreen.dart';
import 'package:digital_krishi/UI/OtherScreens/pendingVerification.dart';
import 'package:digital_krishi/UI/OtherScreens/profileSetup.dart';
import 'package:digital_krishi/UI/PostScreens/postDetails.dart';
import 'package:digital_krishi/UI/UserScreens/userMainScreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

void main() =>
    runApp(MaterialApp(debugShowCheckedModeBanner: false, home: MyApp()));

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String userEmail;
  String loggedInUserType;
  String isFirstTime;
  String isVerified;
  String userId;
  @override
  void initState() {
    super.initState();
    FirebaseAuth.instance.currentUser().then((firebaseUser) {
      if (firebaseUser != null) {
        setState(() {
          userEmail = firebaseUser.email;
          userId = firebaseUser.uid;
          print("Logged in user email:- " + userEmail);
        });
        getUserType();
      }
    });
  }

  void getUserType() async {
    await Firestore.instance
        .collection('users')
        .document(userId)
        .get()
        .then((DocumentSnapshot ds) {
      setState(() {
        loggedInUserType = ds.data['userType'];
        isFirstTime = ds.data['isFirstTime'];
        isVerified = ds.data['isVerified'];
      });
      // use ds as a snapshot
      print("User type:- " + loggedInUserType);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: _getLandingPage(),
      routes: <String, WidgetBuilder>{
        '/LoginScreen': (BuildContext context) => LoginScreen(),
        '/SignUpScreen': (BuildContext context) => SignUpScreen(),
        '/UserMainScreen': (BuildContext context) => UserMainScreen(),
        '/ExpertMainScreen': (BuildContext context) => ExpertMainScreen(),
        '/AdminMainScreen': (BuildContext context) => AdminMainScreen(),
        '/ProfileSetup': (BuildContext context) => ProfileSetup(),
        '/PostDetail': (BuildContext context) => PostDetails(),
        '/PendingVerification': (BuildContext context) => PendingVerification(),
      },
    );
  }

  Widget _getLandingPage() {
    if (userEmail != null) {
      if (loggedInUserType == "Expert") {
        if (isFirstTime == "yes") {
          return ProfileSetup();
        } else {
          if (isVerified == "Verified") {
            return ExpertMainScreen();
          } else {
            return PendingVerification();
          }
        }
      } else if (loggedInUserType == "Farmer") {
        return UserMainScreen();
      } else if (loggedInUserType == "Admin") {
        return AdminMainScreen();
      } else {
        return Scaffold(
          body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: Text(
                    "Checking Your Account Information",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                CircularProgressIndicator(
                  backgroundColor: Colors.red,
                ),
              ],
            ),
          ),
        );
      }
    } else {
      return LoginScreen();
    }
  }
}
