import 'package:digital_krishi/UI/ChatScreens/allUsers.dart';
import 'package:digital_krishi/UI/PostScreens/newPost.dart';
import 'package:digital_krishi/UI/UserScreens/moreSettings.dart';
import 'package:digital_krishi/UI/UserScreens/userHomeScreen.dart';
import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:line_icons/line_icons.dart';

class UserMainScreen extends StatefulWidget {
  @override
  _UserMainScreenState createState() => _UserMainScreenState();
}

class _UserMainScreenState extends State<UserMainScreen> {
  int _selectedIndex = 0;
  String userId;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> _widgetOptions = <Widget>[
      UserHomeScreen(),
      NewPost(),
      AllUsers(),
      MoreSettings(),
    ];
    return Scaffold(
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(color: Colors.white54, boxShadow: [
          BoxShadow(blurRadius: 70, color: Colors.black.withOpacity(.5))
        ]),
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 8),
            child: GNav(
                gap: 0,
                activeColor: Colors.white,
                iconSize: 25,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                duration: Duration(milliseconds: 600),
                tabBackgroundColor: Colors.orange,
                tabs: [
                  GButton(
                    icon: LineIcons.home,
                    text: 'Home',
                  ),
                  GButton(
                    icon: LineIcons.newspaper_o,
                    text: 'Add post',
                  ),
                  GButton(icon: Icons.chat, text: "Chat"),
                  GButton(
                    icon: Icons.more_horiz,
                    text: 'More',
                  ),
                ],
                selectedIndex: _selectedIndex,
                onTabChange: (index) {
                  setState(() {
                    _selectedIndex = index;
                  });
                }),
          ),
        ),
      ),
    );
  }
}
