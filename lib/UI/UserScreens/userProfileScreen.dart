import 'dart:async';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:digital_krishi/UI/PostScreens/postDetails.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';

class UserProfileScreen extends StatefulWidget {
  @override
  _UserProfileScreenState createState() => _UserProfileScreenState();
}

class _UserProfileScreenState extends State<UserProfileScreen> {
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final emailController = TextEditingController();
  final contactController = TextEditingController();
  final addressController = TextEditingController();
  final userTypeController = TextEditingController();
  final isVerifiedController = TextEditingController();

  String photoUrl;
  String userEmail;
  bool isLoading = false;
  String userId;
  File avatarImageFile;
  String imgUrl;
  final FocusNode focusNodeFirstName = FocusNode();
  final FocusNode focusNodelastName = FocusNode();
  final FocusNode focusNodeEmail = FocusNode();
  final FocusNode focusNodeContact = FocusNode();
  final FocusNode focusNodeAddress = FocusNode();
  final FocusNode focusNodeIsVerified = FocusNode();

  @override
  void initState() {
    super.initState();

    userData();
  }

  Future<String> userData() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();
    final String email = user.email.toString();
    final String uID = user.uid;
    this.setState(() {
      userEmail = email;
      userId = uID;
    });

    return email;
  }

  Future getImage() async {
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (image != null) {
      setState(() {
        avatarImageFile = image;
        isLoading = true;
      });
    }
    uploadFile();
  }

  //delete previous pic
  Future deleteImage() async {
    await FirebaseStorage.instance
        .getReferenceFromUrl(photoUrl)
        .then((value) => value.delete())
        .catchError((onError) {
      print(onError);
    });
  }

  Future uploadFile() async {
    String fileName = basename(avatarImageFile.path);
    StorageReference reference =
        FirebaseStorage.instance.ref().child('user_profile').child(fileName);
    StorageUploadTask uploadTask = reference.putFile(avatarImageFile);
    StorageTaskSnapshot storageTaskSnapshot;
    uploadTask.onComplete.then((value) {
      if (value.error == null) {
        storageTaskSnapshot = value;
        storageTaskSnapshot.ref.getDownloadURL().then((downloadUrl) {
          deleteImage();
          photoUrl = downloadUrl;

          Firestore.instance.collection('users').document(userId).updateData({
            'firstName': firstNameController.text.trim(),
            'lastName': lastNameController.text.trim(),
            'nickName': firstNameController.text.trim() +
                ' ' +
                lastNameController.text.trim(),
            'email': emailController.text.trim(),
            'contact': contactController.text.trim(),
            'address': addressController.text.trim(),
            'photoUrl': photoUrl,
            'userType': userTypeController.text.trim(),
            'isVerified': isVerifiedController.text.trim()
          }).then((data) async {
            setState(() {
              isLoading = false;
            });
            Fluttertoast.showToast(msg: "Upload success");
          }).catchError((err) {
            setState(() {
              isLoading = false;
            });
            Fluttertoast.showToast(msg: err.toString());
          });
        }, onError: (err) {
          setState(() {
            isLoading = false;
          });
          Fluttertoast.showToast(msg: 'This file is not an image');
        });
      } else {
        setState(() {
          isLoading = false;
        });
        Fluttertoast.showToast(msg: 'This file is not an image');
      }
    }, onError: (err) {
      setState(() {
        isLoading = false;
      });
      Fluttertoast.showToast(msg: err.toString());
    });
  }

  void handleUpdateData() {
    focusNodeFirstName.unfocus();
    focusNodelastName.unfocus();
    focusNodeEmail.unfocus();
    focusNodeContact.unfocus();
    focusNodeAddress.unfocus();

    setState(() {
      isLoading = true;
    });

    Firestore.instance.collection('users').document(userId).updateData({
      'firstName': firstNameController.text.trim(),
      'lastName': lastNameController.text.trim(),
      'nickName': firstNameController.text.trim() +
          ' ' +
          lastNameController.text.trim(),
      'email': emailController.text.trim(),
      'contact': contactController.text.trim(),
      'address': addressController.text.trim(),
      'photoUrl': photoUrl,
      'userType': userTypeController.text.trim(),
      'isVerified': isVerifiedController.text.trim()
    }).then((data) async {
      setState(() {
        isLoading = false;
        Fluttertoast.showToast(
            msg: "Update success",
            textColor: Colors.red,
            backgroundColor: Colors.black);
      });
    }).catchError((err) {
      setState(() {
        isLoading = false;
        Fluttertoast.showToast(msg: err.toString());
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget _buildProfileListItem(BuildContext context, snapshot) {
      firstNameController.text = snapshot.data.data['firstName'];
      lastNameController.text = snapshot.data.data['lastName'];
      emailController.text = snapshot.data.data['email'];
      contactController.text = snapshot.data.data['contact'];
      addressController.text =
          addressController.text = snapshot.data.data['address'];
      userTypeController.text = snapshot.data.data['userType'];

      photoUrl = snapshot.data.data["photoUrl"];

      //for events
      Widget _buildListItemPosts(BuildContext context,
          DocumentSnapshot document, String collectionName) {
        return InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PostDetails(
                          postImage: document['PostImage'],
                          postTitle: document['PostTitle'],
                          postDescription: document['PostDescription'],
                          postedAt: document['PostedAt'],
                          postedBy: document['PostedBy'],
                          id: document.documentID,
                          collectionName: collectionName,
                        )),
              );
            },
            child: Container(
              margin: EdgeInsets.all(8),
              width: MediaQuery.of(context).size.width / 2.2,
              height: MediaQuery.of(context).size.height / 2,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0),
                  boxShadow: [
                    new BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.5),
                      blurRadius: 20.0,
                    ),
                  ]),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: Container(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: CachedNetworkImage(
                          imageUrl: document['PostImage'],
                          width: MediaQuery.of(context).size.width,
                          fit: BoxFit.cover,
                          placeholder: (context, url) =>
                              CircularProgressIndicator(),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      margin: EdgeInsets.only(left: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            document['PostTitle'],
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          ),
                          Text(
                            "Date: " + document['PostedAt'].toString(),
                            style: TextStyle(color: Colors.black54),
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () async {
                      await Firestore.instance
                          .collection('posts')
                          .document(document.documentID)
                          .delete();

                      await FirebaseStorage.instance
                          .getReferenceFromUrl(document['PostImage'])
                          .then((value) => value.delete())
                          .catchError((onError) {
                        print(onError);
                      });

                      Fluttertoast.showToast(
                          msg: "Deleted Successfully",
                          toastLength: Toast.LENGTH_LONG,
                          backgroundColor: Colors.green,
                          textColor: Colors.white);
                    },
                    child: Container(
                      margin: EdgeInsets.only(left: 10, bottom: 10),
                      alignment: Alignment.center,
                      height: 40,
                      width: 80,
                      decoration: BoxDecoration(
                          color: Colors.redAccent,
                          border: Border.all(width: 0.5),
                          borderRadius: BorderRadius.circular(10)),
                      child: Text(
                        "Delete",
                        style: TextStyle(fontSize: 16, color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ));
      }

      return Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              children: <Widget>[
                // Avatar
                Container(
                  child: Center(
                    child: Stack(
                      children: <Widget>[
                        (avatarImageFile == null)
                            ? (photoUrl != ''
                                ? Material(
                                    child: CachedNetworkImage(
                                      placeholder: (context, url) => Container(
                                        child: CircularProgressIndicator(
                                          strokeWidth: 2.0,
                                          valueColor:
                                              AlwaysStoppedAnimation<Color>(
                                                  Color(0xfff5a623)),
                                        ),
                                        width: 150.0,
                                        height: 150.0,
                                        padding: EdgeInsets.all(20.0),
                                      ),
                                      imageUrl: photoUrl,
                                      width: 150.0,
                                      height: 150.0,
                                      fit: BoxFit.cover,
                                    ),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20.0)),
                                    clipBehavior: Clip.hardEdge,
                                  )
                                : Icon(
                                    Icons.account_circle,
                                    size: 90.0,
                                    color: Colors.black38,
                                  ))
                            : Material(
                                child: Image.file(
                                  avatarImageFile,
                                  width: 90.0,
                                  height: 90.0,
                                  fit: BoxFit.cover,
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(45.0)),
                                clipBehavior: Clip.hardEdge,
                              ),
                        Positioned(
                          left: 45,
                          bottom: -10,
                          child: IconButton(
                            icon: Icon(
                              Icons.camera_alt,
                              color: Colors.blue,
                            ),
                            onPressed: getImage,
                            highlightColor: Color(0xffaeaeae),
                            iconSize: 30.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                  width: double.infinity,
                  margin: EdgeInsets.all(20.0),
                ),

                // Input
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            flex: 2,
                            child: Container(
                              child: Theme(
                                data: Theme.of(context)
                                    .copyWith(primaryColor: Color(0xff203152)),
                                child: TextFormField(
                                  decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black, width: 2.0),
                                    ),
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.red, width: 10.0),
                                    ),
                                    hintText: 'First Name',
                                    labelText: 'First Name',
                                    labelStyle: TextStyle(
                                        fontStyle: FontStyle.italic,
                                        fontWeight: FontWeight.bold,
                                        color: Color(0xff203152)),
                                    contentPadding: EdgeInsets.all(5.0),
                                    hintStyle:
                                        TextStyle(color: Color(0xffaeaeae)),
                                  ),
                                  controller: firstNameController,
                                  focusNode: focusNodeFirstName,
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 2,
                            child: Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Theme(
                                data: Theme.of(context)
                                    .copyWith(primaryColor: Color(0xff203152)),
                                child: TextFormField(
                                  decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black, width: 2.0),
                                    ),
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.red, width: 10.0),
                                    ),
                                    hintText: 'Last Name',
                                    labelText: 'Last Name',
                                    labelStyle: TextStyle(
                                        fontStyle: FontStyle.italic,
                                        fontWeight: FontWeight.bold,
                                        color: Color(0xff203152)),
                                    contentPadding: EdgeInsets.all(5.0),
                                    hintStyle:
                                        TextStyle(color: Color(0xffaeaeae)),
                                  ),
                                  controller: lastNameController,
                                  focusNode: focusNodelastName,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    SizedBox(
                      height: 30,
                    ),

                    // Email
                    Tooltip(
                      waitDuration: Duration(seconds: 5),
                      message: "You cannot change your email",
                      child: Container(
                        child: Theme(
                          data: Theme.of(context)
                              .copyWith(primaryColor: Color(0xff203152)),
                          child: TextFormField(
                            enabled: false,
                            decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.black, width: 2.0),
                              ),
                              border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.red, width: 10.0),
                              ),
                              hintText: 'Email',
                              labelText: 'Email',
                              labelStyle: TextStyle(
                                  fontStyle: FontStyle.italic,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xff203152)),
                              contentPadding: EdgeInsets.all(5.0),
                              hintStyle: TextStyle(color: Color(0xffaeaeae)),
                            ),
                            controller: emailController,
                            focusNode: focusNodeEmail,
                          ),
                        ),
                        margin: EdgeInsets.only(left: 30.0, right: 30.0),
                      ),
                    ),

                    SizedBox(
                      height: 30,
                    ),
                    //Contact
                    Container(
                      child: Theme(
                        data: Theme.of(context)
                            .copyWith(primaryColor: Color(0xff203152)),
                        child: TextFormField(
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black, width: 2.0),
                            ),
                            border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 10.0),
                            ),
                            hintText: 'Contact',
                            labelText: 'Contact',
                            labelStyle: TextStyle(
                                fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.bold,
                                color: Color(0xff203152)),
                            contentPadding: EdgeInsets.all(5.0),
                            hintStyle: TextStyle(color: Color(0xffaeaeae)),
                          ),
                          controller: contactController,
                          focusNode: focusNodeContact,
                        ),
                      ),
                      margin: EdgeInsets.only(left: 30.0, right: 30.0),
                    ),

                    SizedBox(
                      height: 30,
                    ),
                    //address
                    Container(
                      child: Theme(
                        data: Theme.of(context)
                            .copyWith(primaryColor: Color(0xff203152)),
                        child: TextFormField(
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black, width: 2.0),
                            ),
                            border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 10.0),
                            ),
                            hintText: 'Address',
                            labelText: 'Address',
                            labelStyle: TextStyle(
                                fontStyle: FontStyle.italic,
                                fontWeight: FontWeight.bold,
                                color: Color(0xff203152)),
                            contentPadding: EdgeInsets.all(5.0),
                            hintStyle: TextStyle(color: Color(0xffaeaeae)),
                          ),
                          controller: addressController,
                          focusNode: focusNodeAddress,
                        ),
                      ),
                      margin: EdgeInsets.only(left: 30.0, right: 30.0),
                    ),

                    SizedBox(
                      height: 30,
                    ),

                    Container(
                      child: Theme(
                        data: Theme.of(context)
                            .copyWith(primaryColor: Color(0xff203152)),
                        child: TextFormField(
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.black, width: 2.0),
                              ),
                              border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.red, width: 10.0),
                              ),
                              enabled: false,
                              contentPadding: EdgeInsets.all(5.0),
                              hintStyle: TextStyle(color: Color(0xffaeaeae)),
                              labelStyle: TextStyle(
                                  fontStyle: FontStyle.italic,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xff203152)),
                              labelText: 'Account Type'),
                          controller: userTypeController,

                          // focusNode: focusNodeAddress,
                        ),
                      ),
                      margin: EdgeInsets.only(left: 30.0, right: 30.0),
                    ),
                  ],
                  crossAxisAlignment: CrossAxisAlignment.start,
                ),

                // Button
                InkWell(
                  onTap: handleUpdateData,
                  child: Container(
                    margin: EdgeInsets.only(top: 20),
                    width: MediaQuery.of(context).size.width / 1.5,
                    height: 50,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.orange[400],
                      border: Border.all(width: 1),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                    ),
                    child: Text("Update".toUpperCase()),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'My Posts',
                        style: TextStyle(fontSize: 20),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height / 3,
                        child: StreamBuilder(
                          stream: Firestore.instance
                              .collection('posts')
                              .snapshots(),
                          builder: (context, snapshot) {
                            if (!snapshot.hasData) {
                              return LinearProgressIndicator(
                                backgroundColor: Colors.green,
                              );
                            } else {
                              var userPastEvents = [];
                              for (int i = 0;
                                  i < snapshot.data.documents.length;
                                  i++) {
                                if (snapshot.data.documents[i]["PostedBy"] ==
                                    userEmail) {
                                  userPastEvents
                                      .add(snapshot.data.documents[i]);
                                }
                              }
                              return ListView.builder(
                                // itemExtent: 200,
                                scrollDirection: Axis.horizontal,
                                itemCount: userPastEvents.length,
                                itemBuilder: (context, index) =>
                                    _buildListItemPosts(context,
                                        userPastEvents[index], 'pastEvents'),
                              );
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),

          // Loading
          Positioned(
            child: isLoading
                ? Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                      child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Color(0xfff5a623))),
                    ),
                    color: Colors.black.withOpacity(0.5),
                  )
                : Container(),
          ),
        ],
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'UserProfile',
          style:
              TextStyle(color: Color(0xff203152), fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(
            Icons.chevron_left,
          ),
        ),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: StreamBuilder(
                      stream: Firestore.instance
                          .collection('users')
                          .document(userId)
                          .snapshots(),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData)
                          return CircularProgressIndicator(
                            backgroundColor: Colors.green,
                          );
                        return _buildProfileListItem(context, snapshot);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
