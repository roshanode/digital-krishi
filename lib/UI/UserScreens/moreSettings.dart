import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:digital_krishi/UI/OtherScreens/marketPrice.dart';
import 'package:digital_krishi/UI/UserScreens/mapPage.dart';
import 'package:digital_krishi/UI/UserScreens/userProfileScreen.dart';
import 'package:digital_krishi/UI/UserScreens/weatherUpdate.dart';
import 'nearByMarket.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MoreSettings extends StatefulWidget {
  @override
  _MoreSettingsState createState() => _MoreSettingsState();
}

class _MoreSettingsState extends State<MoreSettings> {
  String userId;

  @override
  void initState() {
    super.initState();
    FirebaseAuth.instance.currentUser().then((firebaseUser) async {
      if (firebaseUser != null) {
        setState(() {
          userId = firebaseUser.uid;
        });
      }
    });
  }

  Widget _buildProfileCard(BuildContext context, snapshot) {
    final String email = snapshot.data.data['email'];
    final String nickName = snapshot.data.data['nickName'];
    final String photoUrl = snapshot.data.data['photoUrl'];
    return Container(
      margin: EdgeInsets.only(top: 0.0, left: 20, right: 20.0),
      height: 150,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white,
        border: Border.all(width: 0.3, color: Colors.black),
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 5,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: Container(
                child: (photoUrl != null)
                    ? CachedNetworkImage(
                        imageUrl: photoUrl,
                        fit: BoxFit.cover,
                        placeholder: (context, url) =>
                            CircularProgressIndicator(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      )
                    : Container(
                        child: Align(
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.blue,
                            strokeWidth: 2,
                          ),
                        ),
                      ),
              ),
            ),
          ),
          Expanded(
            flex: 8,
            child: Padding(
              padding: const EdgeInsets.only(left: 30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: (nickName != null)
                        ? (Text(
                            nickName,
                            style: TextStyle(
                              fontSize: 20.0,
                              color: Color.fromARGB(0xff, 32, 168, 74),
                            ),
                          ))
                        : Container(
                            child: Align(
                              alignment: Alignment.center,
                              child: CircularProgressIndicator(
                                backgroundColor: Colors.blue,
                                strokeWidth: 2,
                              ),
                            ),
                          ),
                  ),
                  Container(
                    child: (email != null)
                        ? (Text(
                            email,
                            style: TextStyle(
                              fontSize: 12.0,
                              color: Colors.black,
                            ),
                          ))
                        : Container(
                            child: Align(
                              alignment: Alignment.center,
                              child: CircularProgressIndicator(
                                backgroundColor: Colors.blue,
                                strokeWidth: 1,
                              ),
                            ),
                          ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(right: 15.0),
              child: IconButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => UserProfileScreen()));
                },
                icon: Icon(
                  Icons.edit,
                  size: 30.0,
                  color: Colors.black,
                ),
              ))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color.fromARGB(0xff, 32, 168, 74),
      ),
      backgroundColor: Colors.black12,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: 80.0,
                  color: Color.fromARGB(0xff, 32, 168, 74),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      child: StreamBuilder(
                        stream: Firestore.instance
                            .collection('users')
                            .document(userId)
                            .snapshots(),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData)
                            return CircularProgressIndicator(
                              backgroundColor: Colors.green,
                            );
                          return _buildProfileCard(context, snapshot);
                        },
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => WeatherUpdate(),
                          ),
                        );
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 20),
                        width: MediaQuery.of(context).size.width / 1.1,
                        decoration: BoxDecoration(
                            border: Border.all(width: 0.5),
                            borderRadius: BorderRadius.circular(10)),
                        child: ListTile(
                          leading: Image.asset(
                            'lib/images/rain.png',
                            height: 40,
                            width: 40,
                          ),
                          title: Text(
                            "View the latest weather update",
                            style: TextStyle(
                                fontWeight: FontWeight.w700, fontSize: 20),
                          ),
                          trailing: Icon(Icons.chevron_right),
                        ),
                      ),
                    ),
                    InkWell(
                      splashColor: Colors.green,
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MarketPrice()));
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 20),
                        width: MediaQuery.of(context).size.width / 1.1,
                        decoration: BoxDecoration(
                            border: Border.all(width: 0.5),
                            borderRadius: BorderRadius.circular(10)),
                        child: ListTile(
                          leading: SvgPicture.asset(
                            'lib/images/food.svg',
                            height: 40,
                            width: 40,
                          ),
                          title: Text(
                            "Today's market price for vegetables",
                            style: TextStyle(
                                fontWeight: FontWeight.w700, fontSize: 20),
                          ),
                          trailing: Icon(Icons.chevron_right),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => NearByMarket(),
                          ),
                        );
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 20),
                        width: MediaQuery.of(context).size.width / 1.1,
                        decoration: BoxDecoration(
                            border: Border.all(width: 0.5),
                            borderRadius: BorderRadius.circular(10)),
                        child: ListTile(
                          leading: SvgPicture.asset(
                            'lib/images/fruit.svg',
                            height: 40,
                            width: 40,
                          ),
                          title: Text(
                            "View the nearby vegetable market",
                            style: TextStyle(
                                fontWeight: FontWeight.w700, fontSize: 20),
                          ),
                          trailing: Icon(Icons.chevron_right),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => MapPage(),
                          ),
                        );
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 20),
                        width: MediaQuery.of(context).size.width / 1.1,
                        decoration: BoxDecoration(
                            border: Border.all(width: 0.5),
                            borderRadius: BorderRadius.circular(10)),
                        child: ListTile(
                          title: Text(
                            "Map testing",
                            style: TextStyle(
                                fontWeight: FontWeight.w700, fontSize: 20),
                          ),
                          trailing: Icon(Icons.chevron_right),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
