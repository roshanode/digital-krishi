import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:digital_krishi/UI/AdminScreens/adminHomeScreen.dart';
import 'package:digital_krishi/UI/AdminScreens/verificationPending.dart';
import 'package:digital_krishi/UI/ChatScreens/allUsers.dart';
import 'package:digital_krishi/UI/ExpertScreens/expertHomeScreen.dart';
import 'package:digital_krishi/UI/ExpertScreens/expertProfileScreen.dart';
import 'package:digital_krishi/UI/PostScreens/newPost.dart';
import 'package:digital_krishi/UI/UserScreens/moreSettings.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:line_icons/line_icons.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  String userEmail;
  String loggedInUserType;

  @override
  void initState() {
    super.initState();
    FirebaseAuth.instance.currentUser().then((firebaseUser) {
      if (firebaseUser != null) {
        setState(() {
          userEmail = firebaseUser.email;
        });
        getUserType();
      }
    });
  }

  void getUserType() async {
    await Firestore.instance
        .collection('users')
        .document(userEmail)
        .get()
        .then((DocumentSnapshot ds) {
      setState(() {
        loggedInUserType = ds.data['userType'];
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (loggedInUserType == "Farmer") {
      int _selectedIndex = 0;
      final List<Widget> _widgetOptions = <Widget>[
        NewPost(),
        AllUsers(),
        MoreSettings(),
      ];
      return Scaffold(
        body: _widgetOptions.elementAt(_selectedIndex),
        bottomNavigationBar: Container(
          decoration: BoxDecoration(color: Colors.white54, boxShadow: [
            BoxShadow(blurRadius: 70, color: Colors.black.withOpacity(.5))
          ]),
          child: SafeArea(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 8),
              child: GNav(
                  gap: 0,
                  activeColor: Colors.white,
                  iconSize: 25,
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  duration: Duration(milliseconds: 600),
                  tabBackgroundColor: Colors.orange,
                  tabs: [
                    GButton(
                      icon: LineIcons.newspaper_o,
                      text: 'Add post',
                    ),
                    GButton(icon: Icons.chat, text: "Chat"),
                    GButton(
                      icon: Icons.more_horiz,
                      text: 'More',
                    ),
                  ],
                  selectedIndex: _selectedIndex,
                  onTabChange: (index) {
                    setState(() {
                      _selectedIndex = index;
                    });
                  }),
            ),
          ),
        ),
      );
    } else if (loggedInUserType == "Expert") {
      int _selectedIndex = 0;
      final List<Widget> _widgetOptions = <Widget>[
        ExpertHomeScreen(),
        ExpertProfileScreen(),
      ];
      return Scaffold(
        body: _widgetOptions.elementAt(_selectedIndex),
        bottomNavigationBar: Container(
          decoration: BoxDecoration(color: Colors.white54, boxShadow: [
            BoxShadow(blurRadius: 70, color: Colors.black.withOpacity(.5))
          ]),
          child: SafeArea(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 8),
              child: GNav(
                  gap: 5,
                  activeColor: Colors.white,
                  iconSize: 25,
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  duration: Duration(milliseconds: 600),
                  tabBackgroundColor: Colors.orange,
                  tabs: [
                    GButton(
                      icon: LineIcons.home,
                      text: 'Home',
                    ),
                    GButton(
                      icon: LineIcons.user,
                      text: 'Profile',
                    ),
                  ],
                  selectedIndex: _selectedIndex,
                  onTabChange: (index) {
                    setState(() {
                      _selectedIndex = index;
                    });
                  }),
            ),
          ),
        ),
      );
    } else if (loggedInUserType == "Admin") {
      int _selectedIndex = 0;
      final List<Widget> _widgetOptions = <Widget>[
        AdminHomeScreen(),
        VerificationPending()
      ];
      return Scaffold(
        body: _widgetOptions.elementAt(_selectedIndex),
        bottomNavigationBar: Container(
          decoration: BoxDecoration(color: Colors.white54, boxShadow: [
            BoxShadow(blurRadius: 70, color: Colors.black.withOpacity(.5))
          ]),
          child: SafeArea(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 8),
              child: GNav(
                  gap: 5,
                  activeColor: Colors.white,
                  iconSize: 25,
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  duration: Duration(milliseconds: 600),
                  tabBackgroundColor: Colors.orange,
                  tabs: [
                    GButton(
                      icon: LineIcons.users,
                      text: 'All Users',
                    ),
                    GButton(
                      icon: Icons.verified_user,
                      text: 'Verify Users',
                    ),
                  ],
                  selectedIndex: _selectedIndex,
                  onTabChange: (index) {
                    setState(() {
                      _selectedIndex = index;
                    });
                  }),
            ),
          ),
        ),
      );
    } else {
      return Scaffold(
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: Text(
                  "Checking Your Account Information",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
              CircularProgressIndicator(
                backgroundColor: Colors.red,
              ),
            ],
          ),
        ),
      );
    }
  }
}
