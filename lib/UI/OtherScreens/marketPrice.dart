import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class MarketPrice extends StatefulWidget {
  @override
  _MarketPriceState createState() => _MarketPriceState();
}

class _MarketPriceState extends State<MarketPrice> {
  bool isLoading = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            WebView(
                initialUrl:
                    'https://nepalicalendar.rat32.com/vegetable/embed.php',
                javascriptMode: JavascriptMode.unrestricted,
                onPageFinished: (String finished) {
                  setState(() {
                    isLoading = false;
                  });
                }),
            isLoading
                ? Container(
                    child: Center(
                      child: CircularProgressIndicator(
                        backgroundColor: Colors.white,
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
