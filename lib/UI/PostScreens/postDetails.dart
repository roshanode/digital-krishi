import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:digital_krishi/UI/PostScreens/fullPhoto.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:timeago/timeago.dart' as timeago;

class PostDetails extends StatefulWidget {
  final String postImage;
  final String postTitle;
  final String postDescription;
  final String postedAt;

  final String postedBy;
  final String id;
  final String collectionName;
  PostDetails(
      {Key key,
      this.postImage,
      this.postTitle,
      this.postDescription,
      this.postedAt,
      this.postedBy,
      this.collectionName,
      this.id})
      : super(key: key);
  @override
  _PostDetailsState createState() => _PostDetailsState();
}

class _PostDetailsState extends State<PostDetails> {
  final commentController = TextEditingController();
  final databaseReference = Firestore.instance;
  String userEmail;
  String uId;
  String image;
  String loggedInUserType;
  @override
  void initState() {
    super.initState();
    userData();
  }

  Future<String> userData() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();

    this.setState(() {
      userEmail = user.email;
      uId = user.uid;
    });
    getUserType();
    return uId;
  }

  void getUserType() async {
    await Firestore.instance
        .collection('users')
        .document(uId)
        .get()
        .then((DocumentSnapshot ds) {
      setState(() {
        loggedInUserType = ds.data['userType'];
        image = ds.data['photoUrl'];
      });
    });
  }

  getPostDetail() {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => PostDetails(
                postImage: widget.postImage,
                postTitle: widget.postTitle,
                postDescription: widget.postDescription,
                postedAt: widget.postedAt,
                postedBy: widget.postedBy,
                id: widget.id,
                collectionName: widget.collectionName,
              )),
    );
  }

  @override
  Widget build(BuildContext context) {
    String id1 = widget.id;

    void postComment() async {
      if (commentController.text.isEmpty) {
        Fluttertoast.showToast(
            msg: "Write Something", backgroundColor: Colors.red);
      } else {
        setState(() {
          FocusScope.of(context).requestFocus(new FocusNode());
        });
        await Firestore.instance
            .collection("Comment")
            .document(id1)
            .collection("comments")
            .add({
          "commentedBy": userEmail,
          "comment": commentController.text,
          "image": image,
          "time": DateTime.now()
        });
        setState(() {
          commentController.clear();
          Fluttertoast.showToast(
              msg: "Posted Successfully", backgroundColor: Colors.green);
        });
      }
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black38,
        centerTitle: true,
        title: Text("Test Details"),
      ),
      backgroundColor: Color.fromARGB(0xff, 241, 241, 254),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(12.0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10.0),
                    boxShadow: [
                      new BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.08),
                        blurRadius: 20.0,
                      ),
                    ]),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        widget.postTitle,
                        style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Divider(
                        color: Colors.black,
                      ),
                      Text(
                        "Posted By: \n" + widget.postedBy,
                        style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                      Text(
                        widget.postedAt,
                        style: TextStyle(
                          fontSize: 15.0,
                          color: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Stack(
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  FullPhoto(url: widget.postImage)));
                    },
                    child: Container(
                      height: MediaQuery.of(context).size.width,
                      width: MediaQuery.of(context).size.height,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          boxShadow: [
                            new BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.08),
                              blurRadius: 20.0,
                            ),
                          ]),
                      margin: EdgeInsets.only(top: 20, left: 12, right: 12),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: CachedNetworkImage(
                          fit: BoxFit.cover,
                          width: MediaQuery.of(context).size.width,
                          placeholder: (context, url) =>
                              CircularProgressIndicator(),
                          imageUrl: widget.postImage,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.all(12.0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10.0),
                        boxShadow: [
                          new BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.08),
                            blurRadius: 20.0,
                          ),
                        ]),
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Description : -".toUpperCase(),
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            widget.postDescription,
                            style: TextStyle(fontSize: 15.0),
                            textAlign: TextAlign.justify,
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.all(12.0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10.0),
                        boxShadow: [
                          new BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.08),
                            blurRadius: 20.0,
                          ),
                        ]),
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Column(
                        children: <Widget>[
                          Text(
                            'Comment Section',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Container(
                            height: MediaQuery.of(context).size.height / 3,
                            child: StreamBuilder(
                                stream: Firestore.instance
                                    .collection('Comment')
                                    .document(id1)
                                    .collection('comments')
                                    .orderBy("time", descending: false)
                                    .snapshots(),
                                builder: (context, snapshot) {
                                  if (!snapshot.hasData) {
                                    return Container(
                                      child: Center(
                                        child: Column(
                                          children: <Widget>[
                                            CircularProgressIndicator(
                                              backgroundColor: Colors.green,
                                            ),
                                            Text("No comments"),
                                          ],
                                        ),
                                      ),
                                    );
                                  }
                                  List<Comment> comments = [];
                                  snapshot.data.documents.forEach((doc) {
                                    comments.add(Comment.fromDocument(doc));
                                  });
                                  return ListView(
                                    children: comments,
                                  );
                                }),
                          ),
                          Container(
                            padding: EdgeInsets.only(top: 10),
                            width: MediaQuery.of(context).size.width,
                            child: Wrap(
                              children: <Widget>[
                                Container(
                                  height: 50,
                                  width:
                                      MediaQuery.of(context).size.width / 1.7,
                                  child: TextFormField(
                                    controller: commentController,
                                    cursorColor: Colors.black,
                                    decoration: new InputDecoration(
                                        hintText: "Add Comments",
                                        border: OutlineInputBorder()),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                  width: 10,
                                ),
                                RaisedButton(
                                  elevation: 10,
                                  child: Padding(
                                    padding: const EdgeInsets.all(6.0),
                                    child: Text("Post Comment"),
                                  ),
                                  onPressed: () {
                                    postComment();
                                  },
                                  color: Colors.blue[800],
                                  textColor: Colors.white,
                                  padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                  splashColor: Colors.grey,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Comment extends StatelessWidget {
  final String email;

  final String image;
  final String comment;
  final Timestamp timestamp;

  Comment({this.email, this.image, this.comment, this.timestamp});

  factory Comment.fromDocument(DocumentSnapshot doc) {
    return Comment(
      email: doc['commentedBy'],
      image: doc['image'],
      comment: doc['comment'],
      timestamp: doc['time'],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text(email),
          leading: CircleAvatar(
            backgroundImage: CachedNetworkImageProvider(image),
          ),
          subtitle: Text(comment),
          trailing: Text(timeago.format(timestamp.toDate())),
        ),
        Divider(
          thickness: 1,
          color: Colors.black,
        ),
      ],
    );
  }
}
