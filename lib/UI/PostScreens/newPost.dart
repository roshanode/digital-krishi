import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:path/path.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

class NewPost extends StatefulWidget {
  @override
  _NewPostState createState() => _NewPostState();
}

class _NewPostState extends State<NewPost> {
  String userEmail;
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    userData();
  }

  Future<String> userData() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();
    final String email = user.email;
    this.setState(() {
      userEmail = email;
    });

    return email;
  }

  TextEditingController postTitle = TextEditingController();
  TextEditingController postDescription = TextEditingController();
  TextEditingController postDate = TextEditingController(
      text: DateFormat(' EEE d MMM').format(DateTime.now()));

  final databaseReference = Firestore.instance;
  String imgUrl;
  File _image;
  String _errorMessage;

  String formattedDate = DateFormat.yMMMMEEEEd().format(DateTime.now());
  void _post(BuildContext context) async {
    if (postTitle != null && postDescription != null && _image != null) {
      setState(() {
        _errorMessage = "";
        _isLoading = true;
      });

      try {
        String fileName = basename(_image.path);
        StorageReference firebaseStorageRef = FirebaseStorage.instance
            .ref()
            .child('post_pictures')
            .child(fileName);
        StorageUploadTask uploadTask = firebaseStorageRef.putFile(_image);
        var downUrl = await (await uploadTask.onComplete).ref.getDownloadURL();
        var url = downUrl.toString();
        setState(() {
          imgUrl = url;
        });

        await databaseReference.collection('posts').document().setData(
          {
            'PostedBy': userEmail,
            'PostTitle': postTitle.text.trim(),
            'PostDescription': postDescription.text.trim(),
            'PostImage': imgUrl,
            'PostedAt': formattedDate,
            'Comments': FieldValue.arrayUnion([
              {"commentBy": "", "comment": ""}
            ]),
          },
        );
        setState(() {
          _isLoading = false;
          Fluttertoast.showToast(msg: 'Posted Successfully');
          postTitle.clear();
          postDescription.clear();
          _image = null;
        });
        _showAlert(context);
      } catch (e) {
        print('Error: $e');
        setState(() {
          _isLoading = false;
          _errorMessage = e.message;
          Fluttertoast.showToast(msg: _errorMessage);
        });
      }
    } else {
      Fluttertoast.showToast(msg: 'Please enter the details');
    }
  }

  takePicture() async {
    File image = await ImagePicker.pickImage(source: ImageSource.camera);
//    File img = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      _image = image;
      setState(() {});
    }
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      if (image != null) {
        _image = image;
      }
    });
  }

  void _showAlert(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Your event added successfully !"),
          actions: <Widget>[
            FlatButton(
              child: Text("Okay"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(0xff, 241, 241, 254),
      appBar: AppBar(
        centerTitle: true,
        title: Text('Add Post Upadte'),
        backgroundColor: Colors.black38,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Form(
                key: _formKey,
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 10),
                      TextFormField(
                        enabled: false,
                        controller: postDate,
                        onSaved: (postDate) => postDate = postDate.trim(),
                        maxLines: 1,
                        decoration: InputDecoration(
                          labelText: "Post Date",
                          hintText: "Post Date",
                          border: OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(height: 15),
                      TextFormField(
                        controller: postTitle,
                        onSaved: (postTitle) => postTitle = postTitle.trim(),
                        decoration: InputDecoration(
                          labelText: "Post Title",
                          hintText: "Post Title",
                          border: OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(height: 15),
                      TextFormField(
                        controller: postDescription,
                        onSaved: (postDescription) =>
                            postDescription = postDescription.trim(),
                        maxLines: 10,
                        decoration: InputDecoration(
                          labelText: "Post Description",
                          hintText: "Post Description",
                          border: OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(height: 15),
                      Container(
                        height: MediaQuery.of(context).size.height / 2.5,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            border: Border.all(width: 1, color: Colors.grey),
                            borderRadius: BorderRadius.circular(5)),
                        child: (_image != null)
                            ? Image.file(_image)
                            : Container(
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text("Choose or take a picture"),
                                ),
                              ),
                      ),
                      SizedBox(height: 15),
                      Wrap(
                        runSpacing: 10,
                        children: <Widget>[
                          InkWell(
                            onTap: () {
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                              takePicture();
                            },
                            child: Container(
                              height: 30,
                              width: MediaQuery.of(context).size.width / 2,
                              decoration: BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius: BorderRadius.circular(5)),
                              child: Center(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Icon(
                                      Icons.photo_camera,
                                      size: 20.0,
                                    ),
                                    Text(
                                      'Open Camera',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14.0),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              FocusScope.of(context)
                                  .requestFocus(new FocusNode());
                              getImage();
                            },
                            child: Container(
                                height: 30,
                                width: MediaQuery.of(context).size.width / 2,
                                decoration: BoxDecoration(
                                    color: Colors.green,
                                    borderRadius: BorderRadius.circular(5)),
                                child: Center(
                                    child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Icon(
                                      Icons.file_upload,
                                      size: 20.0,
                                    ),
                                    Text(
                                      'Open Gallery',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14.0),
                                    ),
                                  ],
                                ))),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      InkWell(
                        onTap: () {
                          FocusScope.of(context).requestFocus(new FocusNode());
                          _post(context);
                        },
                        child: Container(
                            height: 50,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                color: Colors.orange,
                                borderRadius: BorderRadius.circular(5)),
                            child: Center(
                                child: Text(
                              'Post',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20.0),
                            ))),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            _isLoading
                ? Positioned.fill(
                    child: Container(
                      height: 100,
                      width: 100,
                      color: Colors.black.withOpacity(0.5),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircularProgressIndicator(
                              backgroundColor: Colors.green,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "Uploading! Please Wait :)",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
