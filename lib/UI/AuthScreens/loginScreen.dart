import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final _formKeyReset = GlobalKey<FormState>();
  bool _obscureText = true;
  String email;
  String password;
  String emailReset;
  String _errorMessage;
  String loggedInUserType;
  String isFirstTime;
  String isVerified;
  bool _isLoading = false;
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  // Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  // Check if form is valid before perform login
  bool _validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Future<String> signIn(String email, String password) async {
    FirebaseUser user = (await _firebaseAuth.signInWithEmailAndPassword(
            email: email, password: password))
        .user;
    if (user.isEmailVerified) {
      return user.uid;
    } else {
      return null;
    }
  }

  // Perform login or signup
  void _validateAndSubmit() async {
    if (_validateAndSave()) {
      setState(() {
        _errorMessage = "";
        _isLoading = true;
      });
      String userId = "";

      try {
        userId = await signIn(email, password);

        if (userId != null) {
          await Firestore.instance
              .collection('users')
              .document(userId)
              .get()
              .then((DocumentSnapshot ds) {
            setState(() {
              loggedInUserType = ds.data['userType'];
              isFirstTime = ds.data['isFirstTime'];
              isVerified = ds.data['isVerified'];
            });
            // use ds as a snapshot
          });

          Fluttertoast.showToast(
              msg: "LoginSuccess",
              toastLength: Toast.LENGTH_SHORT,
              backgroundColor: Colors.blue,
              textColor: Colors.white);
        } else {
          Fluttertoast.showToast(
              msg: "Email Not Verified Yet",
              toastLength: Toast.LENGTH_LONG,
              backgroundColor: Colors.red,
              textColor: Colors.black);
        }
      } catch (e) {
        setState(() {
          _isLoading = false;

          _errorMessage = e.message;
          Fluttertoast.showToast(
              msg: _errorMessage,
              toastLength: Toast.LENGTH_SHORT,
              backgroundColor: Colors.red,
              textColor: Colors.white);
        });
      }
      setState(() {
        _isLoading = false;
      });

      if (userId.length > 0 && userId != null) {
        if (loggedInUserType == "Expert") {
          if (isFirstTime == "yes") {
            Navigator.of(context).pushReplacementNamed('/ProfileSetup');
          } else {
            if (isVerified == "Verified") {
              Navigator.of(context).pushReplacementNamed('/ExpertMainScreen');
            } else {
              Navigator.of(context)
                  .pushReplacementNamed('/PendingVerification');
            }
          }
        } else if (loggedInUserType == "Farmer") {
          Navigator.of(context).pushReplacementNamed('/UserMainScreen');
        } else if (loggedInUserType == "Admin") {
          Navigator.of(context).pushReplacementNamed('/AdminMainScreen');
        } else {
          Navigator.of(context).pushReplacementNamed('/LoginScreen');
        }
      }
    }
  }

//for password reset
  bool _validateAndSaveReset() {
    final form = _formKeyReset.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmitReset() async {
    if (_validateAndSaveReset()) {
      setState(() {
        _errorMessage = "";
      });

      try {
        sendPasswordResetMail(emailReset);

        setState(
          () {
            Fluttertoast.showToast(
                msg:
                    "Email sent to,$emailReset follow the link to reset your password",
                toastLength: Toast.LENGTH_LONG,
                backgroundColor: Colors.grey,
                textColor: Colors.white);

            Navigator.of(context).pushReplacementNamed('/LoginScreen');
          },
        );
      } catch (e) {
        print('Error: $e');
        setState(() {
          _isLoading = false;
          _errorMessage = e.message;
          Fluttertoast.showToast(
              msg: _errorMessage,
              toastLength: Toast.LENGTH_LONG,
              backgroundColor: Colors.red,
              textColor: Colors.white);
        });
      }
    }
  }

//send password reset mail
  Future<void> sendPasswordResetMail(String emailReset) async {
    await _firebaseAuth.sendPasswordResetEmail(email: emailReset);

    return null;
  }

//for password forget dialogue
  void _forgetPassword() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 20),
              width: MediaQuery.of(context).size.width / 1.2,
              height: 200,
              padding: EdgeInsets.only(top: 4, left: 16, right: 16, bottom: 4),
              child: Form(
                key: _formKeyReset,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                          "*Please enter your mail where the password reset email will be sent !"),
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.black, width: 2.0),
                        ),
                        border: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.red, width: 10.0),
                        ),
                        prefixIcon: Icon(
                          Icons.email,
                          color: Colors.grey,
                        ),
                        hintText: 'Email',
                      ),
                      validator: validateEmail,
                      onSaved: (value) => emailReset = value.trim(),
                    ),
                    InkWell(
                      onTap: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        _validateAndSubmitReset();
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 20),
                        height: 50,
                        width: MediaQuery.of(context).size.width / 1.2,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [Color(0xFFf45d27), Color(0xFFf5851f)],
                            ),
                            borderRadius:
                                BorderRadius.all(Radius.circular(50))),
                        child: Center(
                          child: Text(
                            'Send Password reset Email'.toUpperCase(),
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
          duration: Duration(milliseconds: 800),
          content: Text("Tap back again to exit"),
        ),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Stack(
            children: <Widget>[
              Form(
                key: _formKey,
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height / 2.4,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                Color.fromARGB(0xff, 103, 178, 111),
                                Color.fromARGB(0xff, 76, 162, 205)
                              ],
                            ),
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(90))),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Spacer(),
                            Align(
                                alignment: Alignment.center,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(40),
                                  child: Image.asset(
                                    'lib/images/logo.png',
                                    color: Colors.greenAccent,
                                    height: 200,
                                    width: 200,
                                  ),
                                )),
                            Spacer(),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Padding(
                                  padding: const EdgeInsets.only(
                                      bottom: 32, right: 32),
                                  child: Text(
                                    'Login'.toUpperCase(),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 24,
                                        fontWeight: FontWeight.bold),
                                  )),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 40),
                        width: MediaQuery.of(context).size.width / 1.2,
                        child: TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black, width: 2.0),
                            ),
                            border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 10.0),
                            ),
                            prefixIcon: Icon(
                              Icons.email,
                              color: Colors.grey,
                            ),
                            labelText: 'Email',
                            hintText: 'Email',
                          ),
                          validator: validateEmail,
                          onSaved: (value) => email = value.trim(),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 40),
                        width: MediaQuery.of(context).size.width / 1.2,
                        child: TextFormField(
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.black, width: 2.0),
                              ),
                              border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.red, width: 10.0),
                              ),
                              prefixIcon: Icon(
                                Icons.vpn_key,
                                color: Colors.grey,
                              ),
                              suffixIcon: IconButton(
                                  icon: FaIcon(
                                    _obscureText
                                        ? FontAwesomeIcons.eyeSlash
                                        : FontAwesomeIcons.eye,
                                    color: Colors.green,
                                  ),
                                  onPressed: _toggle),
                              hintText: 'Password',
                              labelText: 'Password'),
                          obscureText: _obscureText,
                          validator: validatePassword,
                          onSaved: (value) => password = value.trim(),
                        ),
                      ),
                      Tooltip(
                        message: "You can now reset your passworsd",
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 10, right: 50),
                            child: GestureDetector(
                              onTap: () {
                                _forgetPassword();
                                // Navigator.of(context).pushReplacementNamed('/forgetPassword');
                              },
                              child: Text(
                                'Forgot Password ?',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          left: 20,
                          right: 20,
                          top: 20,
                          bottom: 20,
                        ),
                        child: InkWell(
                          onTap: () {
                            _validateAndSubmit();
                            FocusScope.of(context)
                                .requestFocus(new FocusNode());
                          },
                          child: Container(
                            height: 45,
                            width: MediaQuery.of(context).size.width / 1.2,
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  colors: [
                                    Color(0xFF5a3f37),
                                    Color(0xFF2c7744)
                                  ],
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(50))),
                            child: Center(
                                child: Text('Login',
                                    style: TextStyle(
                                        fontSize: 20.0, color: Colors.white))),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context)
                              .pushReplacementNamed('/SignUpScreen');
                        },
                        child: Container(
                            margin: EdgeInsets.symmetric(vertical: 20),
                            alignment: Alignment.bottomCenter,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  'Don\'t have an account ?',
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  'Register',
                                  style: TextStyle(
                                      color: Colors.green,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            )),
                      )
                    ],
                  ),
                ),
              ),
              _isLoading
                  ? Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.black.withOpacity(0.5),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            CircularProgressIndicator(
                              strokeWidth: 4,
                              backgroundColor: Colors.white,
                            ),
                            Text(
                              "Please Wait..",
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }

  //validator for email
  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  //validator for password
  String validatePassword(String value) {
    if (value.length < 8)
      return 'Password must be 8 character long';
    else
      return null;
  }
}
