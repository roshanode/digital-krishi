import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _formKey = GlobalKey<FormState>();

  String email;
  String password;
  String firstName;
  String lastName;

  String contact;
  String address;
  String _errorMessage;
  String userType = "Farmer";
  bool _isLoading = false;
  bool _obscureText = true;
  String userId;
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
// Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  // Check if form is valid before perform login
  bool _validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  Future<String> signUp(String email, String password) async {
    FirebaseUser user = (await _firebaseAuth.createUserWithEmailAndPassword(
            email: email, password: password))
        .user;
    userId = user.uid;
    return user.uid;
  }

  // Perform login or signup
  void _validateAndSubmit() async {
    if (_validateAndSave()) {
      setState(() {
        _errorMessage = "";
        _isLoading = true;
      });

      try {
        await signUp(email, password);
        FirebaseUser user = await _firebaseAuth.currentUser();
        await user.sendEmailVerification();
        await Firestore.instance.collection('users').document(userId).setData({
          // 'firstName': firstName,
          // 'lastName': lastName,
          'nickName': firstName + " " + lastName,
          'email': email,
          'contact': contact,
          'address': address,
          'photoUrl':
              'https://toppng.com/public/uploads/preview/user-account-management-logo-user-icon-11562867145a56rus2zwu.png',
          'userType': userType,
          'id': userId,
          'isVerified': 'Not Verified',
          'isFirstTime': 'yes',
          'verificationDocument': '',
          'chattingWith': 'none',
          'pushToken': '',
        });
        await FirebaseAuth.instance.signOut();
        await Fluttertoast.showToast(
            msg: "Sign Up Success\nKindly login to continue",
            toastLength: Toast.LENGTH_LONG,
            backgroundColor: Colors.blue,
            textColor: Colors.black);
        Navigator.of(context).pushReplacementNamed('/LoginScreen');
      } catch (e) {
        setState(() {
          _isLoading = false;

          _errorMessage = e.message;
          Fluttertoast.showToast(
              msg: _errorMessage,
              toastLength: Toast.LENGTH_SHORT,
              backgroundColor: Colors.red,
              textColor: Colors.white);
        });
      }
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
          duration: Duration(milliseconds: 1000),
          content: Text("Tap back again to exit"),
        ),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Stack(
            children: <Widget>[
              Form(
                key: _formKey,
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height / 2.4,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  Color.fromARGB(0xff, 103, 178, 111),
                                  Color.fromARGB(0xff, 76, 162, 205)
                                ],
                              ),
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(90))),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Spacer(),
                              Align(
                                alignment: Alignment.center,
                                child: Image.asset(
                                  'lib/images/logo.png',
                                  color: Colors.greenAccent,
                                  height: 200,
                                  width: 200,
                                ),
                              ),
                              Spacer(),
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      bottom: 32, right: 32),
                                  child: Text(
                                    'Register',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 24,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding:
                              EdgeInsets.only(left: 30, right: 30, top: 40),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                flex: 2,
                                child: TextFormField(
                                  decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black, width: 2.0),
                                    ),
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.red, width: 10.0),
                                    ),
                                    prefixIcon: Icon(
                                      Icons.person,
                                      color: Colors.grey,
                                    ),
                                    labelText: 'First Name',
                                    hintText: 'First Name',
                                  ),
                                  validator: validateName,
                                  onSaved: (value) => firstName = value.trim(),
                                ),
                              ),
                              SizedBox(
                                height: 20,
                                width: 20,
                              ),
                              Expanded(
                                flex: 2,
                                child: TextFormField(
                                  decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black, width: 2.0),
                                    ),
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.red, width: 10.0),
                                    ),
                                    labelText: 'Last Name',
                                    hintText: 'Last Name',
                                  ),
                                  validator: validateName,
                                  onSaved: (value) => lastName = value.trim(),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 40),
                          width: MediaQuery.of(context).size.width / 1.2,
                          child: TextFormField(
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.black, width: 2.0),
                              ),
                              border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.red, width: 10.0),
                              ),
                              prefixIcon: Icon(
                                Icons.email,
                                color: Colors.grey,
                              ),
                              labelText: 'Email',
                              hintText: 'Email',
                            ),
                            validator: validateEmail,
                            onSaved: (value) => email = value.trim(),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 40),
                          width: MediaQuery.of(context).size.width / 1.2,
                          child: TextFormField(
                            decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.black, width: 2.0),
                                ),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.red, width: 10.0),
                                ),
                                prefixIcon: Icon(
                                  Icons.vpn_key,
                                  color: Colors.grey,
                                ),
                                suffixIcon: IconButton(
                                    icon: FaIcon(
                                      _obscureText
                                          ? FontAwesomeIcons.eyeSlash
                                          : FontAwesomeIcons.eye,
                                      color: Colors.black,
                                    ),
                                    iconSize: 20,
                                    onPressed: _toggle),
                                hintText: 'Password',
                                labelText: 'Password'),
                            obscureText: _obscureText,
                            validator: validatePassword,
                            onSaved: (value) => password = value.trim(),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 40),
                          width: MediaQuery.of(context).size.width / 1.2,
                          child: TextFormField(
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.black, width: 2.0),
                              ),
                              border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.red, width: 10.0),
                              ),
                              prefixIcon: Icon(
                                Icons.phone,
                                color: Colors.grey,
                              ),
                              labelText: 'Contact',
                              hintText: 'Contact',
                            ),
                            validator: validateMobile,
                            onSaved: (value) => contact = value.trim(),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.2,
                          margin: EdgeInsets.only(top: 40),
                          child: TextFormField(
                            maxLines: 1,
                            decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.black, width: 2.0),
                                ),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.red, width: 10.0),
                                ),
                                labelText: 'Address',
                                hintText: 'Address',
                                prefixIcon: Icon(
                                  Icons.person,
                                  color: Colors.grey,
                                )),
                            validator: validateName,
                            onSaved: (value) => address = value.trim(),
                          ),
                        ),
                        Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              child: Text(
                                "Choose Account Type",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                InkWell(
                                  onTap: () {
                                    this.setState(() {
                                      this.userType = "Farmer";
                                    });
                                  },
                                  child: Stack(
                                    children: <Widget>[
                                      Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(width: 0.5),
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                          color: this.userType == "Farmer"
                                              ? Colors.green
                                              : Colors.white,
                                        ),
                                        padding: EdgeInsets.symmetric(
                                            vertical: 10, horizontal: 15),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Image.asset(
                                              "lib/images/farmer.png",
                                              height: 30.0,
                                              width: 30.0,
                                              color: this.userType == "Farmer"
                                                  ? Colors.white
                                                  : Colors.green,
                                            ),
                                            Text(
                                              "Farmer",
                                              style: TextStyle(
                                                  color:
                                                      this.userType == "Farmer"
                                                          ? Colors.white
                                                          : Colors.green),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                InkWell(
                                  onTap: () {
                                    this.setState(() {
                                      this.userType = "Expert";
                                    });
                                  },
                                  child: Stack(
                                    children: <Widget>[
                                      Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(width: 0.5),
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                          color: this.userType == "Expert"
                                              ? Colors.green
                                              : Colors.white,
                                        ),
                                        padding: EdgeInsets.symmetric(
                                            vertical: 10, horizontal: 15),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Image.asset(
                                              "lib/images/expert.png",
                                              height: 30.0,
                                              width: 30.0,
                                              color: this.userType == "Expert"
                                                  ? Colors.white
                                                  : Colors.green,
                                            ),
                                            Text(
                                              "Expert",
                                              style: TextStyle(
                                                  color:
                                                      this.userType == "Expert"
                                                          ? Colors.white
                                                          : Colors.green),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                left: 20,
                                right: 20,
                                top: 20,
                                bottom: 20,
                              ),
                              child: InkWell(
                                onTap: () {
                                  _validateAndSubmit();
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                },
                                child: Container(
                                  height: 45,
                                  width:
                                      MediaQuery.of(context).size.width / 1.2,
                                  decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                        colors: [
                                          Color(0xFF5a3f37),
                                          Color(0xFF2c7744)
                                        ],
                                      ),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(50))),
                                  child: Center(
                                      child: Text('Sign Up',
                                          style: TextStyle(
                                              fontSize: 20.0,
                                              color: Colors.white))),
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.of(context)
                                    .pushReplacementNamed('/LoginScreen');
                              },
                              child: Container(
                                margin: EdgeInsets.symmetric(vertical: 20),
                                alignment: Alignment.bottomCenter,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      'Already have an account ?',
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      'Login',
                                      style: TextStyle(
                                          color: Colors.green,
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
              _isLoading
                  ? Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.black.withOpacity(0.5),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            CircularProgressIndicator(
                              strokeWidth: 4,
                              backgroundColor: Colors.white,
                            ),
                            Text(
                              "Please Wait..",
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    )
                  : Container()
            ],
          ),
        ),
      ),
    );
  }

  //validators
  //Name validator
  String validateName(String value) {
    if (value.length < 3)
      return 'Name must be more than 2 charater';
    else
      return null;
  }

  //validator for contact number
  String validateMobile(String value) {
// Nepal's Mobile number are of 10 digit only
    if (value.length != 10)
      return 'Mobile Number must be of 10 digit';
    else
      return null;
  }

  //validator for email
  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  //validator for password
  String validatePassword(String value) {
    if (value.length < 8)
      return 'Password must be 8 character long';
    else
      return null;
  }
}
