import 'dart:async';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:digital_krishi/UI/PostScreens/fullPhoto.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';

class ExpertProfileScreen extends StatefulWidget {
  @override
  _ExpertProfileScreenState createState() => _ExpertProfileScreenState();
}

class _ExpertProfileScreenState extends State<ExpertProfileScreen> {
  final fullNameController = TextEditingController();
  final emailController = TextEditingController();
  final contactController = TextEditingController();
  final addressController = TextEditingController();
  final userTypeController = TextEditingController();
  final isVerifiedController = TextEditingController();

  String email;
  String photoUrl;
  String userId;
  bool isLoading = false;
  File avatarImageFile;
  String imgUrl;

  final FocusNode focusNodeFullName = FocusNode();
  final FocusNode focusNodeEmail = FocusNode();
  final FocusNode focusNodeContact = FocusNode();
  final FocusNode focusNodeAddress = FocusNode();
  final FocusNode focusNodeIsVerified = FocusNode();

  @override
  void initState() {
    super.initState();

    userData();
  }

  Future<String> userData() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();

    this.setState(() {
      userId = user.uid;
    });

    return email;
  }

  Future getImage() async {
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (image != null) {
      setState(() {
        avatarImageFile = image;
        isLoading = true;
      });
    }
    uploadFile();
  }

  //delete previous pic
  Future deleteImage() async {
    await FirebaseStorage.instance
        .getReferenceFromUrl(photoUrl)
        .then((value) => value.delete())
        .catchError((onError) {
      print(onError);
    });
  }

  Future uploadFile() async {
    String fileName = basename(avatarImageFile.path);
    StorageReference reference =
        FirebaseStorage.instance.ref().child('user_profile').child(fileName);
    StorageUploadTask uploadTask = reference.putFile(avatarImageFile);
    StorageTaskSnapshot storageTaskSnapshot;
    uploadTask.onComplete.then((value) {
      if (value.error == null) {
        storageTaskSnapshot = value;
        storageTaskSnapshot.ref.getDownloadURL().then((downloadUrl) {
          deleteImage();
          photoUrl = downloadUrl;

          Firestore.instance.collection('users').document(userId).updateData({
            'nickName': fullNameController.text.trim(),
            'contact': contactController.text.trim(),
            'address': addressController.text.trim(),
            'photoUrl': photoUrl,
          }).then((data) async {
            setState(() {
              isLoading = false;
            });
            Fluttertoast.showToast(msg: "Upload success");
          }).catchError((err) {
            setState(() {
              isLoading = false;
            });
            Fluttertoast.showToast(msg: err.toString());
          });
        }, onError: (err) {
          setState(() {
            isLoading = false;
          });
          Fluttertoast.showToast(msg: 'This file is not an image');
        });
      } else {
        setState(() {
          isLoading = false;
        });
        Fluttertoast.showToast(msg: 'This file is not an image');
      }
    }, onError: (err) {
      setState(() {
        isLoading = false;
      });
      Fluttertoast.showToast(msg: err.toString());
    });
  }

  void handleUpdateData() {
    focusNodeFullName.unfocus();
    focusNodeEmail.unfocus();
    focusNodeContact.unfocus();
    focusNodeAddress.unfocus();

    setState(() {
      isLoading = true;
    });

    Firestore.instance.collection('users').document(userId).updateData({
      'nickName': fullNameController.text.trim(),
      'contact': contactController.text.trim(),
      'address': addressController.text.trim(),
      'photoUrl': photoUrl,
    }).then((data) async {
      setState(() {
        isLoading = false;
        Fluttertoast.showToast(
            msg: "Update success",
            textColor: Colors.red,
            backgroundColor: Colors.black);
      });
    }).catchError((err) {
      setState(() {
        isLoading = false;
        Fluttertoast.showToast(msg: err.toString());
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget _buildProfileListItem(BuildContext context, snapshot) {
      fullNameController.text = snapshot.data.data['nickName'];
      emailController.text = snapshot.data.data['email'];
      contactController.text = snapshot.data.data['contact'];
      addressController.text = snapshot.data.data['address'];
      isVerifiedController.text = snapshot.data.data['isVerified'];
      userTypeController.text = snapshot.data.data['userType'];
      photoUrl = snapshot.data.data["photoUrl"];
      imgUrl = snapshot.data.data["verificationDocument"];

      return Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              children: <Widget>[
                // Avatar
                Container(
                  child: Center(
                    child: Stack(
                      children: <Widget>[
                        (avatarImageFile == null)
                            ? (photoUrl != ''
                                ? Material(
                                    child: CachedNetworkImage(
                                      placeholder: (context, url) => Container(
                                        child: CircularProgressIndicator(
                                          strokeWidth: 2.0,
                                          valueColor:
                                              AlwaysStoppedAnimation<Color>(
                                                  Color(0xfff5a623)),
                                        ),
                                        width: 150.0,
                                        height: 150.0,
                                        padding: EdgeInsets.all(20.0),
                                      ),
                                      imageUrl: photoUrl,
                                      width: 150.0,
                                      height: 150.0,
                                      fit: BoxFit.cover,
                                    ),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20.0)),
                                    clipBehavior: Clip.hardEdge,
                                  )
                                : Icon(
                                    Icons.account_circle,
                                    size: 90.0,
                                    color: Colors.black38,
                                  ))
                            : Material(
                                child: Image.file(
                                  avatarImageFile,
                                  width: 90.0,
                                  height: 90.0,
                                  fit: BoxFit.cover,
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(45.0)),
                                clipBehavior: Clip.hardEdge,
                              ),
                        Positioned(
                          left: 45,
                          bottom: -10,
                          child: IconButton(
                            icon: Icon(
                              Icons.camera_alt,
                              color: Colors.blue,
                            ),
                            onPressed: getImage,
                            highlightColor: Color(0xffaeaeae),
                            iconSize: 30.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                  width: double.infinity,
                  margin: EdgeInsets.all(20.0),
                ),

                // Input
                Column(
                  children: <Widget>[
                    // full name
                    Container(
                      child: Text(
                        'Full Name',
                        style: TextStyle(
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff203152)),
                      ),
                      margin:
                          EdgeInsets.only(left: 10.0, bottom: 5.0, top: 10.0),
                    ),
                    Container(
                      child: Theme(
                        data: Theme.of(context)
                            .copyWith(primaryColor: Color(0xff203152)),
                        child: TextField(
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black, width: 2.0),
                            ),
                            border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 10.0),
                            ),
                            hintText: 'Full Name',
                            contentPadding: EdgeInsets.all(5.0),
                            hintStyle: TextStyle(color: Color(0xffaeaeae)),
                          ),
                          controller: fullNameController,
                          focusNode: focusNodeFullName,
                        ),
                      ),
                      margin: EdgeInsets.only(left: 30.0, right: 30.0),
                    ),

                    // Email
                    Container(
                      child: Text(
                        'Email',
                        style: TextStyle(
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff203152)),
                      ),
                      margin:
                          EdgeInsets.only(left: 10.0, top: 30.0, bottom: 5.0),
                    ),
                    Tooltip(
                      waitDuration: Duration(seconds: 5),
                      message: "You cannot change your email",
                      child: Container(
                        child: Theme(
                          data: Theme.of(context)
                              .copyWith(primaryColor: Color(0xff203152)),
                          child: TextField(
                            enabled: false,
                            decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.black, width: 2.0),
                              ),
                              border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.red, width: 10.0),
                              ),
                              hintText: 'Email',
                              contentPadding: EdgeInsets.all(5.0),
                              hintStyle: TextStyle(color: Color(0xffaeaeae)),
                            ),
                            controller: emailController,
                            focusNode: focusNodeEmail,
                          ),
                        ),
                        margin: EdgeInsets.only(left: 30.0, right: 30.0),
                      ),
                    ),
                    //Contact
                    Container(
                      child: Text(
                        'Contact',
                        style: TextStyle(
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff203152)),
                      ),
                      margin:
                          EdgeInsets.only(left: 10.0, bottom: 5.0, top: 10.0),
                    ),
                    Container(
                      child: Theme(
                        data: Theme.of(context)
                            .copyWith(primaryColor: Color(0xff203152)),
                        child: TextField(
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black, width: 2.0),
                            ),
                            border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 10.0),
                            ),
                            hintText: 'Contact',
                            contentPadding: EdgeInsets.all(5.0),
                            hintStyle: TextStyle(color: Color(0xffaeaeae)),
                          ),
                          controller: contactController,
                          focusNode: focusNodeContact,
                        ),
                      ),
                      margin: EdgeInsets.only(left: 30.0, right: 30.0),
                    ),
                    //address
                    Container(
                      child: Text(
                        'Address',
                        style: TextStyle(
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff203152)),
                      ),
                      margin:
                          EdgeInsets.only(left: 10.0, bottom: 5.0, top: 10.0),
                    ),
                    Container(
                      child: Theme(
                        data: Theme.of(context)
                            .copyWith(primaryColor: Color(0xff203152)),
                        child: TextField(
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black, width: 2.0),
                            ),
                            border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 10.0),
                            ),
                            hintText: 'Address',
                            contentPadding: EdgeInsets.all(5.0),
                            hintStyle: TextStyle(color: Color(0xffaeaeae)),
                          ),
                          controller: addressController,
                          focusNode: focusNodeAddress,
                        ),
                      ),
                      margin: EdgeInsets.only(left: 30.0, right: 30.0),
                    ),
                    Container(
                      child: Text(
                        'User Type',
                        style: TextStyle(
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff203152)),
                      ),
                      margin:
                          EdgeInsets.only(left: 10.0, bottom: 5.0, top: 10.0),
                    ),
                    Container(
                      child: Theme(
                        data: Theme.of(context)
                            .copyWith(primaryColor: Color(0xff203152)),
                        child: TextField(
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black, width: 2.0),
                            ),
                            border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 10.0),
                            ),
                            enabled: false,
                            contentPadding: EdgeInsets.all(5.0),
                            hintStyle: TextStyle(color: Color(0xffaeaeae)),
                          ),
                          controller: userTypeController,

                          // focusNode: focusNodeAddress,
                        ),
                      ),
                      margin: EdgeInsets.only(left: 30.0, right: 30.0),
                    ),
                    Container(
                      child: Text(
                        'Is Verified',
                        style: TextStyle(
                            fontStyle: FontStyle.italic,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff203152)),
                      ),
                      margin:
                          EdgeInsets.only(left: 10.0, bottom: 5.0, top: 10.0),
                    ),
                    Container(
                      child: Theme(
                        data: Theme.of(context)
                            .copyWith(primaryColor: Color(0xff203152)),
                        child: TextField(
                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black, width: 2.0),
                            ),
                            border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 10.0),
                            ),
                            enabled: false,
                            contentPadding: EdgeInsets.all(5.0),
                            hintStyle: TextStyle(color: Color(0xffaeaeae)),
                          ),
                          controller: isVerifiedController,

                          // focusNode: focusNodeAddress,
                        ),
                      ),
                      margin: EdgeInsets.only(left: 30.0, right: 30.0),
                    ),
                  ],
                  crossAxisAlignment: CrossAxisAlignment.start,
                ),
                Container(
                  child: Text(
                    'Verification Document',
                    style: TextStyle(
                        fontStyle: FontStyle.italic,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff203152)),
                  ),
                  margin: EdgeInsets.only(left: 10.0, bottom: 5.0, top: 10.0),
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => FullPhoto(url: imgUrl)));
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.width,
                    width: MediaQuery.of(context).size.height,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        boxShadow: [
                          new BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.08),
                            blurRadius: 20.0,
                          ),
                        ]),
                    margin: EdgeInsets.only(top: 20, left: 12, right: 12),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: CachedNetworkImage(
                        fit: BoxFit.cover,
                        width: MediaQuery.of(context).size.width,
                        placeholder: (context, url) =>
                            CircularProgressIndicator(),
                        imageUrl: imgUrl,
                      ),
                    ),
                  ),
                ),
                // Button
                InkWell(
                  onTap: handleUpdateData,
                  child: Container(
                    margin: EdgeInsets.only(top: 20, bottom: 20),
                    width: MediaQuery.of(context).size.width / 1.5,
                    height: 50,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.orange[400],
                      border: Border.all(width: 1),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                    ),
                    child: Text("Update".toUpperCase()),
                  ),
                ),
              ],
            ),
          ),

          // Loading
          Positioned(
            child: isLoading
                ? Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                      child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Color(0xfff5a623))),
                    ),
                    color: Colors.black.withOpacity(0.5),
                  )
                : Container(),
          ),
        ],
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'UserProfile',
        ),
        backgroundColor: Colors.black38,
        centerTitle: true,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: StreamBuilder(
                      stream: Firestore.instance
                          .collection('users')
                          .document(userId)
                          .snapshots(),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData)
                          return CircularProgressIndicator(
                            backgroundColor: Colors.green,
                          );
                        return _buildProfileListItem(context, snapshot);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
