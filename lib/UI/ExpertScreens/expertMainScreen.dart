import 'package:digital_krishi/UI/ChatScreens/allUsers.dart';
import 'package:digital_krishi/UI/ExpertScreens/expertHomeScreen.dart';
import 'package:digital_krishi/UI/ExpertScreens/expertProfileScreen.dart';
import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:line_icons/line_icons.dart';

class ExpertMainScreen extends StatefulWidget {
  @override
  _ExpertMainScreenState createState() => _ExpertMainScreenState();
}

class _ExpertMainScreenState extends State<ExpertMainScreen> {
  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    final List<Widget> _widgetOptions = <Widget>[
      ExpertHomeScreen(),
      AllUsers(),
      ExpertProfileScreen(),
    ];
    return Scaffold(
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(color: Colors.white54, boxShadow: [
          BoxShadow(blurRadius: 70, color: Colors.black.withOpacity(.5))
        ]),
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 8),
            child: GNav(
                gap: 5,
                activeColor: Colors.white,
                iconSize: 25,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                duration: Duration(milliseconds: 600),
                tabBackgroundColor: Colors.orange,
                tabs: [
                  GButton(
                    icon: LineIcons.home,
                    text: 'Home',
                  ),
                  GButton(
                    icon: Icons.message,
                    text: 'Chat',
                  ),
                  GButton(
                    icon: LineIcons.user,
                    text: 'Profile',
                  ),
                ],
                selectedIndex: _selectedIndex,
                onTabChange: (index) {
                  setState(() {
                    _selectedIndex = index;
                  });
                }),
          ),
        ),
      ),
    );
  }
}
