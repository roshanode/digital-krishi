import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:digital_krishi/UI/PostScreens/fullPhoto.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class UserDetail extends StatefulWidget {
  final String profileImage;
  final String fullName;
  final String email;
  final String userId;
  final String contact;
  final String verificationDocument;
  final String userType;
  final String address;
  final String isVerified;
  final String id;
  final String collectionName;

  UserDetail(
      {Key key,
      this.profileImage,
      this.fullName,
      this.email,
      this.userId,
      this.contact,
      this.verificationDocument,
      this.userType,
      this.address,
      this.isVerified,
      this.collectionName,
      this.id})
      : super(key: key);

  @override
  _UserDetailState createState() => _UserDetailState();
}

class _UserDetailState extends State<UserDetail> {
  final commentController = TextEditingController();
  final databaseReference = Firestore.instance;
  String userEmail;
  bool isLoading = false;
  getUserDetail() {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => UserDetail(
                profileImage: widget.profileImage,
                fullName: widget.fullName,
                email: widget.email,
                contact: widget.contact,
                verificationDocument: widget.verificationDocument,
                userType: widget.userType,
                isVerified: widget.isVerified,
                id: widget.id,
                collectionName: widget.collectionName,
              )),
    );
  }

  @override
  Widget build(BuildContext context) {
    String status = widget.isVerified;
    void verifyUser() async {
      setState(() {
        isLoading = true;
      });
      await Firestore.instance
          .collection('users')
          .document(widget.userId)
          .updateData({'isVerified': "Verified"});
      setState(() {
        isLoading = false;
        Fluttertoast.showToast(
            msg: "User verified Successfully",
            textColor: Colors.red,
            backgroundColor: Colors.black);
      });
      Navigator.of(context).pushReplacementNamed('/AdminMainScreen');
    }

    if (widget.userType == "Expert") {
      if (status == "Not Verified") {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.black38,
            centerTitle: true,
            title: Text("User Details"),
          ),
          backgroundColor: Color.fromARGB(0xff, 241, 241, 254),
          body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Stack(
              children: <Widget>[
                SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      FullPhoto(url: widget.profileImage)));
                        },
                        child: Container(
                          height: MediaQuery.of(context).size.width,
                          width: MediaQuery.of(context).size.height,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              boxShadow: [
                                new BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.08),
                                  blurRadius: 20.0,
                                ),
                              ]),
                          margin: EdgeInsets.only(top: 20, left: 12, right: 12),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child: CachedNetworkImage(
                              fit: BoxFit.cover,
                              width: MediaQuery.of(context).size.width,
                              placeholder: (context, url) =>
                                  CircularProgressIndicator(),
                              imageUrl: widget.profileImage,
                            ),
                          ),
                        ),
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.all(12.0),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10.0),
                                boxShadow: [
                                  new BoxShadow(
                                    color: Color.fromRGBO(0, 0, 0, 0.08),
                                    blurRadius: 20.0,
                                  ),
                                ]),
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Description of User Account: -"
                                        .toUpperCase(),
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Divider(),
                                  Text(
                                    "Full Name:- " + widget.fullName,
                                    style: TextStyle(fontSize: 15.0),
                                    textAlign: TextAlign.justify,
                                  ),
                                  Divider(),
                                  Text(
                                    "Email:- " + widget.email,
                                    style: TextStyle(fontSize: 15.0),
                                    textAlign: TextAlign.justify,
                                  ),
                                  Divider(),
                                  Text(
                                    "Contact:- " + widget.contact,
                                    style: TextStyle(fontSize: 15.0),
                                    textAlign: TextAlign.justify,
                                  ),
                                  Divider(),
                                  Text(
                                    "User Type:- " + widget.userType,
                                    style: TextStyle(fontSize: 15.0),
                                    textAlign: TextAlign.justify,
                                  ),
                                  Divider(),
                                  Text(
                                    "verification Status:- " + status,
                                    style: TextStyle(fontSize: 15.0),
                                    textAlign: TextAlign.justify,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => FullPhoto(
                                      url: widget.verificationDocument)));
                        },
                        child: Column(
                          children: <Widget>[
                            Text("verification Document"),
                            Container(
                              height: MediaQuery.of(context).size.width,
                              width: MediaQuery.of(context).size.height,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10.0),
                                  boxShadow: [
                                    new BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.08),
                                      blurRadius: 20.0,
                                    ),
                                  ]),
                              margin:
                                  EdgeInsets.only(top: 20, left: 12, right: 12),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(20),
                                child: CachedNetworkImage(
                                  fit: BoxFit.cover,
                                  width: MediaQuery.of(context).size.width,
                                  placeholder: (context, url) =>
                                      CircularProgressIndicator(),
                                  imageUrl: widget.verificationDocument,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: verifyUser,
                        child: Container(
                          margin: EdgeInsets.only(top: 20, bottom: 20),
                          width: MediaQuery.of(context).size.width / 1.5,
                          height: 50,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: Colors.orange[400],
                            border: Border.all(width: 1),
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                          child: Text("Verify This Account".toUpperCase()),
                        ),
                      ),
                    ],
                  ),
                ),
                isLoading
                    ? Container(
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.black.withOpacity(0.5),
                        child: Center(
                          child: CircularProgressIndicator(
                            strokeWidth: 1,
                            backgroundColor: Colors.redAccent,
                          ),
                        ),
                      )
                    : Container()
              ],
            ),
          ),
        );
      } else {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.black38,
            centerTitle: true,
            title: Text("User Details"),
          ),
          backgroundColor: Color.fromARGB(0xff, 241, 241, 254),
          body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  FullPhoto(url: widget.profileImage)));
                    },
                    child: Container(
                      height: MediaQuery.of(context).size.width,
                      width: MediaQuery.of(context).size.height,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          boxShadow: [
                            new BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.08),
                              blurRadius: 20.0,
                            ),
                          ]),
                      margin: EdgeInsets.only(top: 20, left: 12, right: 12),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: CachedNetworkImage(
                          fit: BoxFit.cover,
                          width: MediaQuery.of(context).size.width,
                          placeholder: (context, url) =>
                              CircularProgressIndicator(),
                          imageUrl: widget.profileImage,
                        ),
                      ),
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.all(12.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10.0),
                            boxShadow: [
                              new BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.08),
                                blurRadius: 20.0,
                              ),
                            ]),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Description of User Account: -".toUpperCase(),
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                              Divider(),
                              Text(
                                "Full Name:- " + widget.fullName,
                                style: TextStyle(fontSize: 15.0),
                                textAlign: TextAlign.justify,
                              ),
                              Divider(),
                              Text(
                                "Email:- " + widget.email,
                                style: TextStyle(fontSize: 15.0),
                                textAlign: TextAlign.justify,
                              ),
                              Divider(),
                              Text(
                                "Contact:- " + widget.contact,
                                style: TextStyle(fontSize: 15.0),
                                textAlign: TextAlign.justify,
                              ),
                              Divider(),
                              Text(
                                "User Type:- " + widget.userType,
                                style: TextStyle(fontSize: 15.0),
                                textAlign: TextAlign.justify,
                              ),
                              Divider(),
                              Text(
                                "verification Status:- " + widget.isVerified,
                                style: TextStyle(fontSize: 15.0),
                                textAlign: TextAlign.justify,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  FullPhoto(url: widget.profileImage)));
                    },
                    child: Column(
                      children: <Widget>[
                        Text("verification Document"),
                        Container(
                          height: MediaQuery.of(context).size.width,
                          width: MediaQuery.of(context).size.height,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              boxShadow: [
                                new BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.08),
                                  blurRadius: 20.0,
                                ),
                              ]),
                          margin: EdgeInsets.only(
                              top: 20, left: 12, right: 12, bottom: 20),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child: CachedNetworkImage(
                              fit: BoxFit.cover,
                              width: MediaQuery.of(context).size.width,
                              placeholder: (context, url) =>
                                  CircularProgressIndicator(),
                              imageUrl: widget.verificationDocument,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      }
    } else {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black38,
          centerTitle: true,
          title: Text("User Details"),
        ),
        backgroundColor: Color.fromARGB(0xff, 241, 241, 254),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                FullPhoto(url: widget.profileImage)));
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.width,
                    width: MediaQuery.of(context).size.height,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        boxShadow: [
                          new BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.08),
                            blurRadius: 20.0,
                          ),
                        ]),
                    margin: EdgeInsets.only(top: 20, left: 12, right: 12),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: CachedNetworkImage(
                        fit: BoxFit.cover,
                        width: MediaQuery.of(context).size.width,
                        placeholder: (context, url) =>
                            CircularProgressIndicator(),
                        imageUrl: widget.profileImage,
                      ),
                    ),
                  ),
                ),
                Column(
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.all(12.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0),
                          boxShadow: [
                            new BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.08),
                              blurRadius: 20.0,
                            ),
                          ]),
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Description of User Account: -".toUpperCase(),
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            ),
                            Divider(),
                            Text(
                              "Full Name:- " + widget.fullName,
                              style: TextStyle(fontSize: 15.0),
                              textAlign: TextAlign.justify,
                            ),
                            Divider(),
                            Text(
                              "Email:- " + widget.email,
                              style: TextStyle(fontSize: 15.0),
                              textAlign: TextAlign.justify,
                            ),
                            Divider(),
                            Text(
                              "Contact:- " + widget.contact,
                              style: TextStyle(fontSize: 15.0),
                              textAlign: TextAlign.justify,
                            ),
                            Divider(),
                            Text(
                              "User Type:- " + widget.userType,
                              style: TextStyle(fontSize: 15.0),
                              textAlign: TextAlign.justify,
                            ),
                            Divider(),
                            Text(
                              "verification Status:- " + widget.isVerified,
                              style: TextStyle(fontSize: 15.0),
                              textAlign: TextAlign.justify,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    }
  }
}
