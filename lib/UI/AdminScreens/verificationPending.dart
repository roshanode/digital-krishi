import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:digital_krishi/UI/AdminScreens/userDetails.dart';

import 'package:flutter/material.dart';

class VerificationPending extends StatefulWidget {
  @override
  _VerificationPendingState createState() => _VerificationPendingState();
}

class _VerificationPendingState extends State<VerificationPending> {
  String userType = "Expert";
  Widget _buildListItemPosts(
      BuildContext context, DocumentSnapshot document, String collectionName) {
    return InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => UserDetail(
                      profileImage: document['photoUrl'],
                      fullName: document['fullName'],
                      email: document['email'],
                      userId: document['id'],
                      contact: document['contact'],
                      userType: document['userType'],
                      verificationDocument: document['verificationDocument'],
                      address: document['address'],
                      isVerified: document["isVerified"],
                      id: document.documentID,
                      collectionName: collectionName,
                    )),
          );
        },
        child: Container(
          margin: EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 10),
          width: MediaQuery.of(context).size.width / 1.6,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20.0),
              boxShadow: [
                new BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.5),
                  blurRadius: 20.0,
                ),
              ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: CachedNetworkImage(
                    imageUrl: document['photoUrl'],
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 4,
                    fit: BoxFit.cover,
                    placeholder: (context, url) => CircularProgressIndicator(),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height / 7,
                margin: EdgeInsets.only(left: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      document['fullName'],
                      overflow: TextOverflow.fade,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                    Text(
                      document['email'],
                      overflow: TextOverflow.fade,
                      style: TextStyle(color: Colors.black),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black38,
        title: Text("Pending Verifications"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.all(4),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  boxShadow: [
                    new BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.5),
                      blurRadius: 20.0,
                    ),
                  ]),
              child: Text(
                'Pending Verification of Experts',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height / 1.26,
              width: MediaQuery.of(context).size.width,
              child: StreamBuilder(
                stream: Firestore.instance.collection('users').snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return LinearProgressIndicator(
                      backgroundColor: Colors.green,
                    );
                  } else {
                    var userPastEvents = [];
                    for (int i = 0; i < snapshot.data.documents.length; i++) {
                      if (snapshot.data.documents[i]["userType"] == "Expert") {
                        if (snapshot.data.documents[i]["isVerified"] ==
                            "Not Verified") {
                          userPastEvents.add(snapshot.data.documents[i]);
                        }
                      }
                    }
                    return ListView.builder(
                      // itemExtent: 200,
                      scrollDirection: Axis.vertical,
                      itemCount: userPastEvents.length,
                      itemBuilder: (context, index) => _buildListItemPosts(
                          context, userPastEvents[index], 'users'),
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
