import 'package:digital_krishi/UI/AdminScreens/adminHomeScreen.dart';
import 'package:digital_krishi/UI/AdminScreens/verificationPending.dart';
import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:line_icons/line_icons.dart';

class AdminMainScreen extends StatefulWidget {
  @override
  _AdminMainScreenState createState() => _AdminMainScreenState();
}

class _AdminMainScreenState extends State<AdminMainScreen> {
  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    final List<Widget> _widgetOptions = <Widget>[
      AdminHomeScreen(),
      VerificationPending()
    ];
    return Scaffold(
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(color: Colors.white54, boxShadow: [
          BoxShadow(blurRadius: 70, color: Colors.black.withOpacity(.5))
        ]),
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 8),
            child: GNav(
                gap: 5,
                activeColor: Colors.white,
                iconSize: 25,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                duration: Duration(milliseconds: 600),
                tabBackgroundColor: Colors.orange,
                tabs: [
                  GButton(
                    icon: LineIcons.users,
                    text: 'All Users',
                  ),
                  GButton(
                    icon: Icons.verified_user,
                    text: 'Verify Users',
                  ),
                ],
                selectedIndex: _selectedIndex,
                onTabChange: (index) {
                  setState(() {
                    _selectedIndex = index;
                  });
                }),
          ),
        ),
      ),
    );
  }
}
